#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include "include/Definitions.h"


int main( int argc, char* args[] )
{
    int posX = 100;
    int posY = 200;
    int sizeX = X_RES_DEFINE*16;
    int sizeY = Y_RES_DEFINE*16;
    bool quit = 0;
    SDL_Window* window;
    SDL_Renderer* renderer;
    SDL_Texture* Font;
    SDL_Event event;

    //FrameBufferClass *WorldBuffer = new FrameBufferClass();

    SDL_Rect *DrawLoc = new SDL_Rect;

    DrawLoc->x = 0;
    DrawLoc->y = 0;
    DrawLoc->w = 16;
    DrawLoc->h = 16;


    // Initialize SDL
    // ==========================================================
    if ( SDL_Init( SDL_INIT_EVERYTHING ) != 0 )
    {
        // Something failed, print error and exit.
        std::cout << " Failed to initialize SDL : " << SDL_GetError() << std::endl;
        return -1;
    }

    // Create and init the window
    // ==========================================================
    window = SDL_CreateWindow( "Server", posX, posY, sizeX, sizeY, 0 );

    if ( window == nullptr )
    {
        std::cout << "Failed to create window : " << SDL_GetError();
        return -1;
    }

    // Create and init the renderer
    // ==========================================================
    renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );

    if ( renderer == nullptr )
    {
        std::cout << "Failed to create renderer : " << SDL_GetError();
        return -1;
    }

    Font = LoadTextureFont(renderer);

    // Initialise Renderer
    // ==========================================================
    // Set size of renderer to the same as window
    SDL_RenderSetLogicalSize( renderer, sizeX, sizeY );

    ClearRenderer (renderer)

    // Render Pix
    SDL_RenderCopy(renderer, Font, GetRectforFont('k'), DrawLoc);

    // Render the changes above ( which up until now had just happened behind the scenes )
    SDL_RenderPresent( renderer);


    while (!quit)
    {
//        t.StartTimer();
        while (SDL_PollEvent(&event))
        {
            if (event.type==SDL_QUIT){quit=true;}
            //if time to go, get ready to leave the game loop
//            event(e);
        }

 //       render();

//        if (t.GetRemaining()>0)
//        {
 //           SDL_Delay(t.GetRemaining());
//        }
    }

    SDL_Quit();
    return 0;
}